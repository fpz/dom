// Package dom contains Go interfaces for DOM Level 1, as defined in
// https://www.w3.org/TR/REC-DOM-Level-1/ . It doesn't claim to be
// complete.
//
// It preserves most of the clumsy naming except for the prefix DOM
// and capitalization. Attributes in the interface have been modeled as
// a getter method with no prefix and a setter method with the prefix
// Set. There are no explicit error return values, methods may panic in
// case of a truly exceptional condition.
//
// All of these interfaces may change!
package dom

import "fmt"

type Exception uint16

const (
	IndexSizeErr             Exception = 1
	DomstringSizeErr         Exception = 2
	HierarchyRequestErr      Exception = 3
	WrongDocumentErr         Exception = 4
	InvalidCharacterErr      Exception = 5
	NoDataAllowedErr         Exception = 6
	NoModificationAllowedErr Exception = 7
	NotFoundErr              Exception = 8
	NotSupportedErr          Exception = 9
	InuseAttributeErr        Exception = 10
)

func (e Exception) Error() string {
	return fmt.Sprint("Dom Exception", uint16(e))
}

type NodeType uint16

const (
	ElementNode               NodeType = 1
	AttributeNode             NodeType = 2
	TextNode                  NodeType = 3
	CDATASectionNode          NodeType = 4
	EntityReferenceNode       NodeType = 5
	EntityNode                NodeType = 6
	ProcessingInstructionNode NodeType = 7
	CommentNode               NodeType = 8
	DocumentNode              NodeType = 9
	DocumentTypeNode          NodeType = 10
	DocumentFragmentNode      NodeType = 11
	NotationNode              NodeType = 12
)

type (
	Implementation interface {
		HasFeature(string, string) bool
	}

	DocumentFragment interface {
		Node
	}

	Document interface {
		Node
		Doctype() DocumentType
		Implementation() Implementation
		DocumentElement() Element
		CreateElement(string) Element
		CreateDocumentFragment() DocumentFragment
		CreateTextNode(string) Text
		CreateComment(string) Comment
		CreateCDATASection(string) CDATASection
		CreateProcessingInstruction(string, string) ProcessingInstruction
		CreateAttribute(string) Attr
		CreateEntityReference(string) EntityReference
	}

	Node interface {
		NodeName() string
		NodeValue() string
		SetNodeValue(string)
		NodeType() NodeType
		ParentNode() Node
		ChildNodes() NodeList
		FirstChild() Node
		LastChild() Node
		PreviousSibling() Node
		NextSibling() Node
		Attributes() NamedNodeMap
		OwnerDocument() Document
		InsertBefore(Node, Node) Node
		ReplaceChild(Node, Node) Node
		RemoveChild(Node) Node
		AppendChild(Node) Node
		HasChildNodes() bool
		CloneNode(bool) Node
	}

	NodeList interface {
		Item(uint64) Node
		Length() uint64
	}

	NamedNodeMap interface {
		Item(uint64) Node
		Length() uint64
		GetNamedItem(string) Node
		SetNamedItem(Node) Node
		RemoveNamedItem(string) Node
	}

	CharacterData interface {
		Node
		Data() string
		SetData(string)
		Length() uint64
		SubstringData(uint64, uint64) string
		AppendData(string)
		InsertData(uint64, string)
		DeleteData(uint64, uint64)
		ReplaceData(uint64, uint64, string)
	}

	Attr interface {
		Node
		Name() string
		Specified() bool
		Value() string
		SetValue(string)
	}

	Element interface {
		Node
		TagName() string
		GetAttribute(string) string
		SetAttribute(string, string)
		RemoveAttribute(string)
		GetAttributeNode(string) Attr
		SetAttributeNode(Attr) Attr
		RemoveAttributeNode(Attr)
		GetElementsByTagName(string) NodeList
		Normalize()
	}

	Text interface {
		CharacterData
		SplitText(uint64) Text
	}

	Comment interface {
		CharacterData
	}

	CDATASection interface {
		Text
	}

	DocumentType interface {
		Node
		Name() string
		Entities() NamedNodeMap
		Notations() NamedNodeMap
	}

	Notation interface {
		Node
		PublicId() string
		SystemId() string
	}

	Entity interface {
		Node
		PublicId() string
		SystemId() string
		NotationName() string
	}

	EntityReference interface {
		Node
	}

	ProcessingInstruction interface {
		Node
		Target() string
		Data() string
		SetData(string)
	}
)
