package htmldom

import (
	"bitbucket.org/fpz/dom"
	"golang.org/x/net/html"
)

// emptyNode provides a basic implementation of dom.Node that does
// nothing.
// FIXME: should probably panic in some cases.
type emptyNode struct{}

func (emptyNode) NodeName() string                         { return "" }
func (emptyNode) NodeValue() string                        { return "" }
func (emptyNode) SetNodeValue(string)                      {}
func (emptyNode) NodeType() dom.NodeType                   { return 0 }
func (emptyNode) ParentNode() dom.Node                     { return nil }
func (emptyNode) ChildNodes() dom.NodeList                 { return nil }
func (emptyNode) FirstChild() dom.Node                     { return nil }
func (emptyNode) LastChild() dom.Node                      { return nil }
func (emptyNode) PreviousSibling() dom.Node                { return nil }
func (emptyNode) NextSibling() dom.Node                    { return nil }
func (emptyNode) Attributes() dom.NamedNodeMap             { return nil }
func (emptyNode) OwnerDocument() dom.Document              { return nil }
func (emptyNode) InsertBefore(dom.Node, dom.Node) dom.Node { return nil }
func (emptyNode) ReplaceChild(dom.Node, dom.Node) dom.Node { return nil }
func (emptyNode) RemoveChild(dom.Node) dom.Node            { return nil }
func (emptyNode) AppendChild(dom.Node) dom.Node            { return nil }
func (emptyNode) HasChildNodes() bool                      { return false }
func (emptyNode) CloneNode(bool) dom.Node                  { return nil }

type attrOf struct {
	emptyNode
	n   *html.Node
	key string
}

func (a attrOf) NodeName() string {
	return a.key
}

func (a attrOf) NodeValue() string {
	return a.Value()
}

func (a attrOf) NodeType() dom.NodeType {
	return dom.AttributeNode
}

func (a attrOf) Name() string {
	return a.key
}

func (a attrOf) Specified() bool {
	for _, attr := range a.n.Attr {
		if attr.Namespace == "" && attr.Key == a.key {
			return true
		}
	}
	return false
}

func (a attrOf) Value() string {
	for _, attr := range a.n.Attr {
		if attr.Namespace == "" && attr.Key == a.key {
			return attr.Val
		}
	}
	return ""
}

func (a attrOf) SetValue(val string) {
	for _, attr := range a.n.Attr {
		if attr.Namespace == "" && attr.Key == a.key {
			attr.Val = val
			return
		}
	}
	a.n.Attr = append(a.n.Attr, html.Attribute{
		Key: a.key,
		Val: val,
	})
}