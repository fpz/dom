package htmldom

import (
	"bitbucket.org/fpz/dom"
	"golang.org/x/net/html"
)

type Element struct{ Node }

func (e Element) TagName() string {
	return h(e).Data
}

func (e Element) GetAttribute(key string) string {
	for _, a := range h(e).Attr {
		if a.Namespace == "" && a.Key == key {
			return a.Val
		}
	}
	return ""
}

func (e Element) SetAttribute(key string, value string) {
	for _, a := range h(e).Attr {
		if a.Namespace == "" && a.Key == key {
			a.Val = value
			return
		}
	}
	h(e).Attr = append(h(e).Attr, html.Attribute{
		Key: key,
		Val: value,
	})
}

func (e Element) RemoveAttribute(key string) {
	attr := h(e).Attr
	for i, a := range attr {
		if a.Namespace == "" && a.Key == key {
			attr[i] = attr[len(attr)-1]
			h(e).Attr = attr[:len(attr)-1]
			return
		}
	}
}

func (e Element) GetAttributeNode(string) dom.Attr {
	panic("not implemented")
}

func (e Element) SetAttributeNode(dom.Attr) dom.Attr {
	panic("not implemented")
}

func (e Element) RemoveAttributeNode(dom.Attr) {
	panic("not implemented")
}

func (e Element) GetElementsByTagName(string) dom.NodeList {
	panic("not implemented")
}

func (e Element) Normalize() {
	panic("not implemented")
}
