package htmldom

import "bitbucket.org/fpz/dom"

// CharacterData is very inefficient, because it
// operates on rune offsets while the underlying
// data is UTF-8.
// BUG: should be UTF-16 offsets.
type CharacterData struct{ Node }

func (c CharacterData) Data() string {
	return h(c).Data
}

func (c CharacterData) SetData(data string) {
	h(c).Data = data
}

func (c CharacterData) Length() uint64 {
	return uint64(len([]rune(c.Data())))
}

func (c CharacterData) SubstringData(off, count uint64) string {
	r := []rune(c.Data())
	return string(r[off : off+count])
}

func (c CharacterData) AppendData(data string) {
	h(c).Data += data
}

func (c CharacterData) InsertData(i uint64, data string) {
	r := []rune(c.Data())
	h(c).Data = string(r[:i]) + data + string(r[i:])
}

func (c CharacterData) DeleteData(off, count uint64) {
	r := []rune(c.Data())
	h(c).Data = string(r[:off]) + string(r[off+count:])
}
func (c CharacterData) ReplaceData(off, count uint64, data string) {
	r := []rune(c.Data())
	h(c).Data = string(r[:off]) + data + string(r[off+count:])
}

type Text struct{ CharacterData }

func (t Text) SplitText(off uint64) dom.Text {
	r := []rune(t.Data())
	t2 := t.CloneNode(false).(Text)
	p := t.ParentNode()
	if sib := t.NextSibling(); sib != nil {
		p.InsertBefore(t2, sib)
	} else {
		p.AppendChild(t2)
	}
	t.SetData(string(r[:off]))
	t2.SetData(string(r[off:]))
	return t2
}

var _ dom.Text = Text{}

type Comment struct{ CharacterData }
