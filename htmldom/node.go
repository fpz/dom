/*
Package htmldom is a simple wrapper around the golang.org/x/net/html
structures to implement the dom interfaces.
*/
package htmldom

import (
	"bitbucket.org/fpz/dom"
	"golang.org/x/net/html"
)

type htmler interface {
	html() *html.Node
}

func h(n dom.Node) *html.Node {
	return n.(htmler).html()
}

func node(h *html.Node) dom.Node {
	if h == nil {
		return nil
	}
	switch h.Type {
	case html.TextNode:
		return Text{CharacterData{Node{h}}}
	case html.CommentNode:
		return Comment{CharacterData{Node{h}}}
	case html.DocumentNode:
		return Document{Node{h}}
	case html.ElementNode:
		return Element{Node{h}}
	default:
		return Node{h}
		// TODO add mode special cases for different types
	}
}

type Node struct{ N *html.Node }

func (n Node) html() *html.Node {
	return n.N
}

func (n Node) NodeName() string {
	switch n.N.Type {
	case html.TextNode:
		return "#text"
	case html.DocumentNode:
		return "#document"
	case html.ElementNode:
		return n.N.Data
	case html.CommentNode:
		return "#comment"
	case html.DoctypeNode:
		return n.N.Data
	}
	return "#error"
}

func (n Node) NodeValue() string {
	switch n.N.Type {
	case html.TextNode, html.CommentNode:
		return n.N.Data
	default:
		return ""
	}
}

func (n Node) SetNodeValue(val string) {
	switch n.N.Type {
	case html.TextNode, html.CommentNode:
		n.N.Data = val
	default:
	}
}

func (n Node) NodeType() dom.NodeType {
	switch n.N.Type {
	case html.TextNode:
		return dom.TextNode
	case html.DocumentNode:
		return dom.DocumentNode
	case html.ElementNode:
		return dom.ElementNode
	case html.CommentNode:
		return dom.CommentNode
	case html.DoctypeNode:
		return dom.DocumentTypeNode
	}
	return 0 // FIXME really??

}

func (n Node) ParentNode() dom.Node {
	return node(n.N.Parent)
}

func (n Node) ChildNodes() dom.NodeList {
	return childrenOf{n.N}
}

func (n Node) FirstChild() dom.Node {
	return node(n.N.FirstChild)
}

func (n Node) LastChild() dom.Node {
	return node(n.N.LastChild)
}

func (n Node) PreviousSibling() dom.Node {
	return node(n.N.PrevSibling)
}

func (n Node) NextSibling() dom.Node {
	return node(n.N.NextSibling)
}

func (n Node) Attributes() dom.NamedNodeMap {
	panic("not implemented")
}

func (n Node) OwnerDocument() dom.Document {
	panic("not implemented")
}

func (n Node) InsertBefore(newC dom.Node, refC dom.Node) dom.Node {
	n.N.InsertBefore(h(newC), h(refC))
	return newC
}

func (n Node) ReplaceChild(newC dom.Node, oldC dom.Node) dom.Node {
	n.N.InsertBefore(h(newC), h(oldC))
	n.N.RemoveChild(h(oldC))
	return oldC
}

func (n Node) RemoveChild(oldC dom.Node) dom.Node {
	n.N.RemoveChild(h(oldC))
	return oldC
}

func (n Node) AppendChild(newC dom.Node) dom.Node {
	n.N.AppendChild(h(newC))
	return newC
}

func (n Node) HasChildNodes() bool {
	return n.N.FirstChild != nil
}

func (n Node) CloneNode(deep bool) dom.Node {
	panic("not implemented")
}
