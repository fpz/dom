package htmldom

import (
	"bitbucket.org/fpz/dom"
	"golang.org/x/net/html"
)

type attributesOf struct{ n *html.Node }

func (a attributesOf) Item(i uint64) dom.Node {
	return attrOf{n: a.n, key: a.n.Attr[i].Key}
}

func (a attributesOf) Length() uint64 {
	return uint64(len(a.n.Attr))
}

func (a attributesOf) GetNamedItem(name string) dom.Node {
	for _, attr := range a.n.Attr {
		if attr.Key == name {
			return attrOf{n: a.n, key: name}
		}
	}
	return nil
}

func (a attributesOf) SetNamedItem(n dom.Node) dom.Node {
	// ???
	return nil
}

func (a attributesOf) RemoveNamedItem(name string) dom.Node {
	n := a.GetNamedItem(name)
	if n != nil {
		for i, attr := range a.n.Attr {
			if attr.Key == name {
				a.n.Attr[i] = a.n.Attr[len(a.n.Attr)-1]
				a.n.Attr = a.n.Attr[:len(a.n.Attr)-1]
				return n
			}
		}
	}
	return n
}