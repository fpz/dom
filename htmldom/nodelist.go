package htmldom

import (
	"bitbucket.org/fpz/dom"
	"golang.org/x/net/html"
)

type childrenOf struct{ N *html.Node }

func (l childrenOf) Item(i uint64) dom.Node {
	var n *html.Node
	for n = l.N.FirstChild; i > 0 && n != nil; n = n.NextSibling {
		i--
	}
	return node(n)
}

func (l childrenOf) Length() uint64 {
	count := uint64(0)
	for n := l.N.FirstChild; n != nil; n = n.NextSibling {
		count++
	}
	return count
}
