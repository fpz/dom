package htmldom

import "bitbucket.org/fpz/dom"

type Document struct{ Node }

func (d Document) Doctype() dom.DocumentType {
	panic("not implemented")
}

func (d Document) Implementation() dom.Implementation {
	panic("not implemented")
}

func (d Document) DocumentElement() dom.Element {
	for n := h(d).FirstChild; n != nil; n = n.NextSibling {
		if e, ok := node(n).(Element); ok {
			return e
		}
	}
	return nil
}

func (d Document) CreateElement(string) dom.Element {
	panic("not implemented")
}
func (d Document) CreateDocumentFragment() dom.DocumentFragment {
	panic("not implemented")
}

func (d Document) CreateTextNode(string) dom.Text {
	panic("not implemented")
}

func (d Document) CreateComment(string) dom.Comment {
	panic("not implemented")
}

func (d Document) CreateCDATASection(string) dom.CDATASection {
	panic("not implemented")
}

func (d Document) CreateProcessingInstruction(string, string) dom.ProcessingInstruction {
	panic("not implemented")
}

func (d Document) CreateAttribute(string) dom.Attr {
	panic("not implemented")
}

func (d Document) CreateEntityReference(string) dom.EntityReference {
	panic("not implemented")
}
